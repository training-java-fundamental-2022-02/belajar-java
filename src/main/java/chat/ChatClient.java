package chat;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ChatClient {
    private String host;
    private Integer port;

    public void start(){
        try (Socket socket = new Socket(host, port);
            BufferedReader readerClient = new BufferedReader(new InputStreamReader(System.in));
            BufferedReader readerServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            PrintWriter writer = new PrintWriter(socket.getOutputStream());
        ) {
            System.out.println("Silahkan ketik pesan anda");
            
            String pesan;
            while((pesan = readerClient.readLine()) != null){
                writer.println(pesan); // tidak langsung kirim, cuma isi buffer saja
                writer.flush(); // untuk mengirim data yang ada di buffer
                String balasan = readerServer.readLine();
                System.out.println("S>"+balasan);
                if("quit".equalsIgnoreCase(balasan)) {
                    break;
                }
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
