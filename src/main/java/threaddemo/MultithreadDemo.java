package threaddemo;

import java.util.ArrayList;
import java.util.List;

public class MultithreadDemo {
    public static void main(String[] args) throws InterruptedException {
        Integer jumlahThread = 5;
        List<CounterDemo> daftarThread = new ArrayList<>();

        for(int i=0; i<jumlahThread; i++){
            CounterDemo cd = new CounterDemo();
            daftarThread.add(cd);
            cd.start();
        }

        // panggil join di semua thread
        for(CounterDemo c : daftarThread){
            c.join();
        }

        System.out.println("Semua thread selesai");
    }

    static class CounterDemo extends Thread {
        private Counter counter = new Counter();
        
        public void run(){
            try {
                counter.hitung();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
