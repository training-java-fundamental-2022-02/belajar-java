package threaddemo;

import java.util.ArrayList;
import java.util.List;

public class ThreadsafeDemo {
    public static void main(String[] args) throws Exception {
        Integer jumlahThread = 500;
        Integer jumlahUlang = 10;
        ThreadsafeCounter c = new ThreadsafeCounter();

        List<TambahCounter> daftarThreadTambah = new ArrayList<>();
        for(int i = 0; i<jumlahThread; i++){
            daftarThreadTambah.add(new TambahCounter(c, jumlahUlang));
        }

        List<KurangCounter> daftarThreadKurang = new ArrayList<>();
        for(int i = 0; i<jumlahThread; i++){
            daftarThreadKurang.add(new KurangCounter(c, jumlahUlang));
        }

        // start semua thread
        for(int i = 0; i<jumlahThread; i++){
            daftarThreadTambah.get(i).start();
            daftarThreadKurang.get(i).start();
        }

        // join ke semua thread
        for(int i = 0; i<jumlahThread; i++){
            daftarThreadTambah.get(i).join();
            daftarThreadKurang.get(i).join();
        }

        System.out.println("============= Posisi Akhir =============");
       c.show();
    }

    static class TambahCounter extends Thread {
        private Integer ulang;
        private ThreadsafeCounter counter;

        public TambahCounter(ThreadsafeCounter c, Integer u){
            this.counter = c;
            this.ulang = u;
        }

        public void run(){
            for(int i=0; i<ulang; i++){
                counter.tambah();
            }
        }
    }

    static class KurangCounter extends Thread {
        private Integer ulang;
        private ThreadsafeCounter counter;

        public KurangCounter(ThreadsafeCounter c, Integer u){
            this.counter = c;
            this.ulang = u;
        }

        public void run(){
            for(int i=0; i<ulang; i++){
                counter.kurang();
            }
        }
    }
}
