package toko;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Setter @Getter
public class PembelianDetail {
    private Produk produk;
    private Integer jumlah;

    /*
    // default constructor
    // otomatis dibuatkan oleh Java SDK
    // kalau kita tidak membuat constructor sendiri
    public PembelianDetail(){

    }
    */

    // kalau sudah buat constructor, maka default constructor tidak akan dibuatkan otomatis
    public PembelianDetail(Produk p, Integer jumlah){
        produk = p;
        this.jumlah = jumlah;
    }

    public BigDecimal subtotal(){
        return produk.getHarga().multiply(new BigDecimal(jumlah));
    }
}
