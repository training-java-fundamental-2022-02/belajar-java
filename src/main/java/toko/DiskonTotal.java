package toko;

import java.math.BigDecimal;

public class DiskonTotal implements Diskon {

    private static final BigDecimal BATAS_DISKON = new BigDecimal(20000000);
    private static final BigDecimal PERSENTASE_DISKON = new BigDecimal(0.1);

    public BigDecimal hitung(Pembelian pembelian){
        if(BATAS_DISKON.compareTo(pembelian.total()) == -1){
            return PERSENTASE_DISKON.multiply(pembelian.total());
        } else {
            return BigDecimal.ZERO;
        }
        // ini tidak akan dijalankan, karena sudah keburu return
        //System.out.println("Tidak jalan, karena sudah return");
        //return null;
    }
    
}
