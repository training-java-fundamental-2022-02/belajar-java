package toko;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URISyntaxException;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class DemoToko {
    public static void main(String[] args) throws URISyntaxException, IOException {
        
        Customer c1 = new Customer();
        c1.setNama("endy");
        c1.setEmail("endy.muhardin@gmail.com");

        ProdukDb databaseProduk = new ProdukDb();
        Produk p1 = databaseProduk.cariByKode("P-001");
        Produk p3 = databaseProduk.cariByKode("P-003");

        PembelianDetail pd1 = new PembelianDetail(p1, 2);
        PembelianDetail pd2 = new PembelianDetail(p3, 1);
        
        Pembelian p = new Pembelian();
        p.setCustomer(c1);
        p.getDaftarPembelianDetail().add(pd1);
        p.getDaftarPembelianDetail().add(pd2);

        NumberFormat formatter = DecimalFormat.getInstance(new Locale("in", "id"));
        System.out.println("Pembelian oleh "+p.getCustomer().getNama());
        System.out.println("============ Rincian ============ ");
        for(PembelianDetail pd : p.getDaftarPembelianDetail()){
            System.out.print("Produk : "+pd.getProduk().getNama());
            System.out.print(" : " +formatter.format(pd.getProduk().getHarga().setScale(0)));
            System.out.println(" x "+pd.getJumlah());
            System.out.println("Subtotal : Rp. "+formatter.format(pd.subtotal().setScale(0)));
            System.out.println("-------------");
        }
        System.out.println("============");
        System.out.println("Total : Rp. "+formatter.format(p.total().setScale(0)));

        // aplikasikan diskon ke pembelian
        p.getDaftarDiskon().add(new DiskonTotal());
        p.getDaftarDiskon().add(new DiskonProduk());
        p.getDaftarDiskon().add(new DiskonCustomer());
        p.getDaftarDiskon().add(new DiskonAkhirBulan());
        System.out.println("Nilai diskon : "+formatter.format(p.totalDiskon().setScale(0, RoundingMode.HALF_EVEN)));

        // menampilkan rincian diskon
        p.tampilkanRincianDiskon();

        // menampilkan yang harus dibayar
        System.out.println("Total Pembayaran : "+formatter.format(p.totalBayar().setScale(0, RoundingMode.HALF_EVEN)));

        // edit data produk
        Produk p2 = databaseProduk.cariByKode("P-003");
        p2.setNama("Laptop HP");
        p2.setHarga(new BigDecimal("11500000"));

        // tambah data produk baru
        Produk p5 = new Produk();
        p5.setKode("P-005");
        p5.setNama("Handphone Oppo");
        p5.setHarga(new BigDecimal(5500000));
        databaseProduk.semuaProduk().add(p5);

        // tulis data ke file
        databaseProduk.save();
    }
}
