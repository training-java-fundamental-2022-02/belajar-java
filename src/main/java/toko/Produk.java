package toko;

import java.math.BigDecimal;

import lombok.Getter;

@Getter
public class Produk {
    private String kode;
    private String nama;
    private String foto;
    private BigDecimal harga;

    public BigDecimal getHarga(){
        return harga;
    }

    public void setKode(String k){
        kode = k;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setHarga(BigDecimal harga){
        this.harga = harga;
    }
}
