package toko;

import java.math.BigDecimal;

public interface Diskon {
    BigDecimal hitung(Pembelian pembelian); // abstract method (tidak ada implementasi, hanya deklarasi saja)
}


// implementasi : 
// 1. Diskon dari Total Pembelian
// 2. Diskon untuk produk tertentu
// 3. Diskon untuk customer yang namanya Agus
// 4. Diskon gajian, 3 hari terakhir setiap bulan 