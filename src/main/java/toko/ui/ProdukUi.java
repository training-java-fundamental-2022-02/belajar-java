package toko.ui;

import java.awt.*;
import java.awt.event.*;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Locale;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import toko.Produk;
import toko.ProdukDb;

public class ProdukUi {

    private ProdukDb db;
    private List<Produk> dataProduk;
    private JTextField txtKode;
    private JTextField txtNama;
    private JFormattedTextField txtHarga;
    private JTable tblProduk;

    public static void main(String[] args) throws Exception {
        new ProdukUi().tampilkanAplikasi();
    }

    public void tampilkanAplikasi() throws Exception {
        JFrame fr = new JFrame();
        fr.setTitle("Aplikasi Toko");
        fr.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        fr.getContentPane().setLayout(new BorderLayout());
        
        JPanel pnlButton = siapkanPanelAtas();
        fr.getContentPane().add(pnlButton, BorderLayout.PAGE_START);

        JPanel pnlInput = siapkanFormInput();
        fr.getContentPane().add(pnlInput, BorderLayout.LINE_END);

        db = new ProdukDb();
        dataProduk = db.semuaProduk();

        tblProduk = new JTable(new ProdukTableModel(dataProduk));
        tblProduk.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tblProduk.getSelectionModel().addListSelectionListener(new TabelProdukListener(this));

        fr.getContentPane().add(new JScrollPane(tblProduk), BorderLayout.CENTER);

        /*
        // Border Layout
        fr.getContentPane().setLayout(new BorderLayout());
        fr.getContentPane().add(btnTambah, BorderLayout.PAGE_START);
        fr.getContentPane().add(btnEdit, BorderLayout.PAGE_END);
        fr.getContentPane().add(btnHapus, BorderLayout.LINE_START);
        fr.getContentPane().add(btnSimpan, BorderLayout.LINE_END);
        fr.getContentPane().add(btnUlang);         
         */

        // Flow Layout
        //fr.getContentPane().setLayout(new FlowLayout());

        // Grid Layout
        //fr.getContentPane().setLayout(new GridLayout(3,2));

        // BoxLayout
        /*
        fr.getContentPane().setLayout(new BoxLayout(fr.getContentPane(), BoxLayout.PAGE_AXIS));
        fr.getContentPane().add(btnTambah);
        fr.getContentPane().add(btnEdit);
        fr.getContentPane().add(btnHapus);
        fr.getContentPane().add(btnSimpan);
        fr.getContentPane().add(btnUlang);         
         */

        // fr.setSize(800, 600);
        fr.pack();
        fr.setLocationRelativeTo(null);
        fr.setVisible(true);
    }

    private void setFormEditable(Boolean editable){
        txtKode.setEditable(editable);
        txtNama.setEditable(editable);
        txtHarga.setEditable(editable);
    }

    private void kosongkanForm(){
        txtKode.setText("");
        txtNama.setText("");
        txtHarga.setText("");
        setFormEditable(false);
    }

    public void isiForm(Produk p){
        txtKode.setText(p.getKode());
        txtNama.setText(p.getNama());
        txtHarga.setValue(p.getHarga());
    }

    public void refreshTabel(){
        ((ProdukTableModel)tblProduk.getModel()).fireTableDataChanged();
    }

    private JPanel siapkanFormInput() {
        // textfield
        txtKode = new JTextField(20);
        txtNama = new JTextField(20);
        txtHarga = new JFormattedTextField(
            DecimalFormat.getCurrencyInstance(
                new Locale("in", "id")));
        txtHarga.setColumns(20);
        txtHarga.setValue(BigDecimal.ZERO);

        JPanel pnlInput = new JPanel();
        pnlInput.setBorder(BorderFactory.createTitledBorder("Input Produk"));
        pnlInput.setLayout(new GridBagLayout());

        GridBagConstraints g1 = new GridBagConstraints();
        g1.gridx = 0;
        g1.gridy = 0;
        g1.weightx = 0.1;
        g1.fill = GridBagConstraints.HORIZONTAL;
        pnlInput.add(new JLabel("Kode"), g1);

        GridBagConstraints g2 = new GridBagConstraints();
        g2.gridx = 0;
        g2.gridy = 1;
        pnlInput.add(new JLabel("Nama"), g2);

        GridBagConstraints g3 = new GridBagConstraints();
        g3.gridx = 0;
        g3.gridy = 2;
        pnlInput.add(new JLabel("Harga"), g3);

        GridBagConstraints g4 = new GridBagConstraints();
        g4.gridx = 1;
        g4.gridy = 0;
        pnlInput.add(txtKode, g4);

        GridBagConstraints g5 = new GridBagConstraints();
        g5.gridx = 1;
        g5.gridy = 1;
        pnlInput.add(txtNama, g5);

        GridBagConstraints g6 = new GridBagConstraints();
        g6.gridx = 1;
        g6.gridy = 2;
        pnlInput.add(txtHarga, g6);

        setFormEditable(false);

        return pnlInput;
    }

    private JPanel siapkanPanelAtas() {
        // button
        JButton btnTambah = new JButton("Tambah");
        JButton btnEdit = new JButton("Edit");
        JButton btnHapus = new JButton("Hapus");
        JButton btnSimpan = new JButton("Simpan");
        JButton btnUlang = new JButton("Ulang");

        // action listener
        btnTambah.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                kosongkanForm();
            }

        });

        btnEdit.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                setFormEditable(true);
            }

        });

        btnHapus.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                int index = tblProduk.getSelectedRow();
                if(index == -1){
                    JOptionPane.showMessageDialog(null, 
                    "Produk belum dipilih", "Error",
                    JOptionPane.ERROR_MESSAGE);
                }
                db.hapus(index);
                ProdukUi.this.kosongkanForm();
                ProdukUi.this.setFormEditable(false);
                ProdukUi.this.refreshTabel();
            }

        });

        btnSimpan.addActionListener(new ActionListener(){

            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedIndex = tblProduk.getSelectedRow();
                Produk p = new Produk();
                if(selectedIndex != -1){
                    p = dataProduk.get(selectedIndex);
                } 
                p.setKode(txtKode.getText());
                p.setNama(txtNama.getText());
                p.setHarga(new BigDecimal((Long)txtHarga.getValue()));
                
                if(selectedIndex == -1){
                    dataProduk.add(p);
                }
                try {
                    db.save();
                } catch (URISyntaxException e1) {
                    e1.printStackTrace();
                }
                refreshTabel();
                kosongkanForm();
                setFormEditable(false);
            }

        });

        JPanel pnlButton = new JPanel();
        pnlButton.setLayout(new GridBagLayout());

        GridBagConstraints gp1 = new GridBagConstraints();
        gp1.gridx = 0;
        gp1.gridy = 0;
        gp1.weightx = 0.1;
        gp1.fill = GridBagConstraints.HORIZONTAL;
        pnlButton.add(btnTambah, gp1);

        GridBagConstraints gp2 = new GridBagConstraints();
        gp2.gridx = 1;
        gp2.gridy = 0;
        gp2.weightx = 0.1;
        gp2.fill = GridBagConstraints.HORIZONTAL;
        pnlButton.add(btnEdit, gp2);

        GridBagConstraints gp3 = new GridBagConstraints();
        gp3.gridx = 2;
        gp3.gridy = 0;
        gp3.weightx = 0.1;
        gp3.fill = GridBagConstraints.HORIZONTAL;
        pnlButton.add(btnHapus, gp3);

        GridBagConstraints gp4 = new GridBagConstraints();
        gp4.gridx = 3;
        gp4.gridy = 0;
        gp4.weightx = 0.1;
        gp4.fill = GridBagConstraints.HORIZONTAL;
        pnlButton.add(btnSimpan, gp4);

        GridBagConstraints gp5 = new GridBagConstraints();
        gp5.gridx = 4;
        gp5.gridy = 0;
        gp5.weightx = 0.1;
        gp5.fill = GridBagConstraints.HORIZONTAL;
        pnlButton.add(btnUlang, gp5);
        return pnlButton;
    }

    class TabelProdukListener implements ListSelectionListener {

        private ProdukUi produkUi;

        public TabelProdukListener(ProdukUi produkUi) {
            this.produkUi = produkUi;
        }

        @Override
        public void valueChanged(ListSelectionEvent e) {
            ListSelectionModel lsm = (ListSelectionModel)e.getSource();
            if(lsm.getMinSelectionIndex() != -1){
                produkUi.isiForm(dataProduk.get(lsm.getMinSelectionIndex()));
            }
        }

    }
}
