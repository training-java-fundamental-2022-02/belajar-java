package toko;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class DiskonProduk implements Diskon {

    private static final List<String> DAFTAR_PRODUK_DISKON 
        = Arrays.asList("P-001", "P-002");
    
    private static final BigDecimal PERSENTASE_DISKON = new BigDecimal(0.15);
    @Override
    public BigDecimal hitung(Pembelian pembelian) {
        BigDecimal hasil = BigDecimal.ZERO;

        for (PembelianDetail pDetail : pembelian.getDaftarPembelianDetail()) {
            Produk p = pDetail.getProduk();
            if(DAFTAR_PRODUK_DISKON.contains(p.getKode())){
                hasil = hasil.add(PERSENTASE_DISKON.multiply(pDetail.subtotal()));
            }
        }

        return hasil;
    }
    
}
